package maxsub

import (
	"math/rand"
	"testing"
	"time"
)

func TestSubsliceMaxSum(t *testing.T) {
	s := []int{-2, -5, 6, -2, -3, 1, 5, -6}
	max := subsliceMaxSum(s)
	if max != 7 {
		t.Errorf("Wrong result!")
	}
}

func BenchmarkSubsliceMaxSum2(b *testing.B) {
	const n = 100000
	s := make([]int, n)
	rand.Seed(time.Now().UTC().UnixNano())
	for i := range s {
		s[i] = rand.Intn(200) - 100
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		subsliceMaxSum(s)
	}
}
