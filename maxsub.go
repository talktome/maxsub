//  https://en.wikipedia.org/wiki/Maximum_subarray_problem
package maxsub

func subsliceMaxSum(s []int) (max int) {
	sum, max := s[0], s[0]
	for _, v := range s[1:] {
		if sum < 0 { // If sum is negative, start new summing from v
			sum = v
		} else {
			sum += v
		}
		if sum > max { // Save biggest sum found
			max = sum
		}
	}
	return
}
